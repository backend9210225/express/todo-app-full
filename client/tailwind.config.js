/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      boxShadow: {
        "3xl":
          "0 0 10px #000000, 0 0 25px #454545, 0 0 50px #5d5d5d, 0 0 200px #888888",
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      gridTemplateColumns: {
        16: "repeat(16, minmax(0, 1fr))",
      },
      gridColumnEnd: {
        13: "13",
        14: "14",
        15: "15",
        16: "16",
        17: "17",
      },
      gridTemplateRows: {
        9: "repeat(9, minmax(0, 1fr))",
      },
      zIndex: {
        "-1": "-1",
      },
      colors: {
        arapawa: {
          50: "#f1f3ff",
          100: "#e6eaff",
          200: "#d0d8ff",
          300: "#aab4ff",
          400: "#7b86ff",
          500: "#454aff",
          600: "#2620ff",
          700: "#160ef3",
          800: "#120bcc",
          900: "#110ba7",
          950: "#030363",
        },
        black: {
          50: "#f6f6f6",
          100: "#e7e7e7",
          200: "#d1d1d1",
          300: "#b0b0b0",
          400: "#888888",
          500: "#6d6d6d",
          600: "#5d5d5d",
          700: "#4f4f4f",
          800: "#454545",
          900: "#3d3d3d",
          950: "#000000",
        },
      },
    },
  },
  plugins: [],
};

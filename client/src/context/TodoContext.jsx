/* eslint-disable react/prop-types */

import { createContext, useState, useEffect } from 'react'

import axios from 'axios'
import { server } from '../api/client'


import { handleDeleteTodo } from '../Utils/deleteTodo'
import { setCompletedTodo } from '../Utils/updateCompletedTodo'
import { setOffCompletedTodo } from '../Utils/setOffCompletedTodod'

const TodoContext = createContext()

function TodoProvider({ children }) {


    const [todos, setTodos] = useState([])

    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);


    const getPosts = async () => {
        const { data } = await axios.get(server.baseUrl)
        console.log(data)
        return data
    }

    const handleDelete = async (id) => {
        await handleDeleteTodo(id)
        const updatedTodos = todos.filter((todo) => todo._id !== id);
        setTodos(updatedTodos);
    }

    const amount = todos.filter(todo => !!todo.completed).length;
    const [openModal, setOpenModal] = useState(false)

    // * search
    const [searchValue, setSearchValue] = useState('')

    // * show todos oncompleted
    const amountTodosCompleted = todos.filter(todo => !!todo.isCompleted).length
    // * show all todos
    const amountTodos = todos.length

    const searchetTodos = todos.filter((todo) => {
        return todo.title.toLowerCase().includes(searchValue.toLowerCase())
    })


    // * set completed todos
    async function completedTodo(data) {
        await setCompletedTodo(data)
        await getPosts().then(data => setTodos(data))
    }

    // * set oncompleted todos  
    async function offCompletedTodo(todo) {
        await setOffCompletedTodo(todo)
        await getPosts().then(data => setTodos(data))
    }


    // * delete todos
    function deleteTodo(id) {
        handleDelete(id)
    }

    useEffect(() => {
        try {
            getPosts().then(data => setTodos(data))
            setLoading(false);
        } catch (error) {
            setLoading(false);
            setError(true);
        }
    }, []);
    const contextValue = {
        todos, setTodos, loading,
        error, setSearchValue, setLoading, amount, setError, amountTodos, amountTodosCompleted, searchetTodos, completedTodo, offCompletedTodo, deleteTodo, searchValue, openModal, setOpenModal
    }
    return (
        <TodoContext.Provider value={contextValue}>
            {children}
        </TodoContext.Provider>
    )
}

export { TodoContext, TodoProvider }


function EmptyTodo() {
    return (
        <>
            <div role="status" className="max-w-full h-52 gap-4  text-center p-4 space-y-4 border border-gray-200  divide-gray-200 rounded shadow animate-pulse dark:divide-gray-700 dark:border-gray-700">
                <div className="flex items-center justify-between gap-2">
                    <div className='flex-1 flex flex-col gap-3 justify-center items-center'>
                        <div className="w-[90%] h-3 bg-gray-300 rounded-full dark:bg-gray-600"></div>
                        <div className="w-[85%] h-3 bg-gray-200 rounded-full dark:bg-gray-700"></div>
                        <div className="w-[90%] h-3 bg-gray-300 rounded-full dark:bg-gray-600"></div>
                    </div>
                    <div className="h-12 bg-gray-300 rounded-md dark:bg-gray-700 w-12"></div>
                    <div className="h-12 bg-gray-300 rounded-md dark:bg-gray-700 w-12"></div>
                    <div className="h-12 bg-gray-300 rounded-md dark:bg-gray-700 w-12"></div>
                </div>
                <div className="uppercase flex-1">Crea tu nuevo todo</div>



            </div>
            
            <div className="flex-1 flex justify-center items-center">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="animate-bounce stroke-cyan-500 w-10 h-10">
                    <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 13.5L12 21m0 0l-7.5-7.5M12 21V3" />
                </svg>
            </div>
        </>
    )
}

export default EmptyTodo
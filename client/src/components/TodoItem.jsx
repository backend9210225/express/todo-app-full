/* eslint-disable react/prop-types */
// import { useState } from "react"
import { Check, Delete, OffChecked } from '../icons/TodoItemIcons'
// onClick={() => { console.log(`clivk en ${text}`)}}
// role="button"
// tabIndex={0}
//aria - label="Toggle todo completed status"
// 4. `onClick`: Define una función anónima que se ejecutará cuando se haga clic en el elemento de lista. Sin embargo, en este caso, no se ha proporcionado ninguna funcionalidad en la función.
// 5. `role` y `tabIndex`: Estos atributos se utilizan para mejorar la accesibilidad en la interfaz de usuario. `role` define el rol del elemento (en este caso, "botón") y `tabIndex` establece el orden en el que el elemento se enfoca cuando se navega utilizando la tecla Tab.
function TodoItem({ text, completed, onComplete, offComplete, onDelete, className }) {

    return (
        <ul className={className}>
            <li className="flex h-32 items-center p-4 rounded-md shadow-md  dark:text-white transition-colors duration-300 ease-in-out"
                style={{
                    textDecoration: completed ? 'line-through' : 'none',
                    textDecorationColor: completed ? 'red' : 'black',
                }}
                key={text}
            >
                <div className="flex-1 flex w-full">
                    <p className="flex-grow text-gray-800 dark:text-white"
                        style={{
                            opacity: completed ? '0.5' : '',
                        }}
                    >{text}</p>
                </div>
                <div className="flex m-0 flex-wrap p-0 justify-end items-center gap-2 max-w-xs btn-container">
                    <button className="bg-blue-600 px-4 py-2 rounded-xl "
                        onClick={onComplete}
                    >
                        <Check />
                    </button>
                    <button className="bg-red-500 px-4 py-2 rounded-xl "
                        onClick={offComplete}
                    >
                        <OffChecked />
                    </button>

                    <button className="bg-red-500 px-4 py-2 rounded-xl "
                        onClick={onDelete}
                    >
                        <Delete />
                    </button>


                </div>
            </li>
        </ul>
    )
}

export default TodoItem
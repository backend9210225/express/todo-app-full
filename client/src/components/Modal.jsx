/* eslint-disable react/prop-types */

import ReactDOM from 'react-dom';

function Modal({ children }) {
    return <div>
        {
            ReactDOM.createPortal(
                <div className="Modal">
                    {children}
                </div>,
                document.getElementById('modal')
            )
        }
    </div>
}

export { Modal };

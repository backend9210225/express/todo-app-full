/* eslint-disable react/prop-types */


function TodoList(props) {
    //console.log(props.children)
    return (
        <div className={`flex flex-1 flex-col gap-6 p-2 borde rounded-2xl scroll-animation glass-bg border-collapse border-4 border-sky-500 overflow-y-scroll `}>
            {props.children}
        </div>
    )
}

export default TodoList
import { server } from "../api/client";
import axios from "axios";

export const createTodo = async (todo) => {
  await axios.post(server.baseUrl, todo);
};

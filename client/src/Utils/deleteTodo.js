// import { server } from "../api/client";
import axios from "axios";
export const handleDeleteTodo = async (id) => {
  const route = `http://localhost:3000/todos/${id}`;
  console.log(route);
  const { status } = await axios.delete(route);
  return status === 200;
};

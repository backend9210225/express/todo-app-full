import axios from "axios";
import { server } from "../api/client";
export const getPosts = async () => {
  const { data } = await axios.get(server.baseUrl);
  console.log(data);
  return data;
};

export const dataTodos = [
  { text: "Cortar cebolla", completed: true, status: "inProgresh" },
  {
    text: "Tomar café, en la competencia de la icpc con mis companieros duramte el contest",
    completed: false,
    status: "finished",
  },
  { text: "Hacer ejercicio", completed: true, status: "inProgresh" },
  { text: "Ir al gimnasio", completed: false, status: "done" },
  { text: "Ir al supermercado", completed: false, status: "finished" },
  { text: "Cortar cebolla", completed: true, status: "done" },
  {
    text: "Tomar café, en la competencia de la icpc con mis companieros duramte el contest",
    completed: false,
    status: "finished",
  },
  { text: "Hacer ejercicio", completed: true, status: "inProgresh" },
  { text: "Ir al gimnasio", completed: false, status: "done" },
  { text: "Ir al supermercado", completed: false, status: "finished" },
];

// localStorage.setItem("todos", JSON.stringify(dataTodos));

export const mapMonth = new Map();
export const mapDay = new Map();

mapMonth.set(0, "January");
mapMonth.set(1, "February");
mapMonth.set(2, "March");
mapMonth.set(3, "April");
mapMonth.set(4, "May");
mapMonth.set(5, "June");
mapMonth.set(6, "July");
mapMonth.set(7, "August");
mapMonth.set(8, "September");
mapMonth.set(9, "October");
mapMonth.set(10, "November");
mapMonth.set(11, "December");

mapDay.set(1, "Monday");
mapDay.set(2, "Tuesday");
mapDay.set(3, "Wednesday");
mapDay.set(4, "Thursday");
mapDay.set(5, "Friday");
mapDay.set(6, "Saturday");
mapDay.set(7, "Sunday");

import axios from "axios";
import { server } from "../api/client";

export const setCompletedTodo = async (todo) => {
  try {
    // Construye la URL de la solicitud PUT utilizando todo._id
    console.log(`${server.baseUrl}/${todo._id}`);
    const { status } = await axios.patch(`${server.baseUrl}/${todo._id}`, {
      "isCompleted": true,
      "status":"Hecho",
    });
    console.log(status);
    return status === 200;
  } catch (error) {
    console.error("Error en la solicitud PUT:", error);
    return false; // Puedes manejar el error de acuerdo a tus necesidades.
  }
};

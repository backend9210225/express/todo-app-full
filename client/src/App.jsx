import { useState } from "react";
import Auth from "./layout/auth";
import Dashboard from "./layout/Dashboard";

function App() {
  const [auth, setAuth] = useState(false);
  // const [showHome, setShowHome] = useState(true);


  const handleData = (eventData) => {
    console.log(eventData);
    if (
      eventData.password === "12345678" &&
      eventData.userName === "alguien@gmail.com"
    ) {
      setAuth(true);
    }
  };

  // useEffect(() => {
  //   window.setTimeout(() => {
  //     setShowHome(true);
  //   }, 7000);
  // }, []);
  return (
    <main>
        {auth ? (
          <div className="container mx-auto">
            <Auth onChildEvent={handleData} />
          </div>
        ) : (
          <div className="">
            {/* <ReactNotifications/> */}
            <Dashboard />
          </div>
        )}
      </main>
  )
}

export default App

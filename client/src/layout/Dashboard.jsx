/* eslint-disable no-unused-vars */
import NavBar from '../components/NavBar'

import { useState, useEffect, useContext } from 'react'

import { CreateIcon, ListIcon, LogoutIcon } from '../icons/DashboardIcons'


// datas utils
import { mapDay, mapMonth, dataTodos } from '../Utils/DataTodos.js'

//Local storage
import { useLocalstorage } from '../hooks/useLocalstorage'

import { TodoProvider } from '../context/TodoContext'
import App from './App'

import Error from "../components/notif/Error";
import Success from "../components/notif/Success";

function Dashboard() {
    // const items = useContext(Context)
    const [dataclock, setDataClock] = useState(new Date());
    useEffect(() => {

        const interval = setInterval(() => {
            setDataClock(new Date())
        }, 1000)
        return () => clearInterval(interval)
    }, [])

    return (
        <>
            <div className='flex md:flex-row flex-col gap-3 w-[99%] h-auto md:h-[100vh] mx-auto py-3'>
                <div className='bg-glass flex-1'>
                    <div className='flex flex-col overflow-hidden relative gap-10 text-center justify-center items-center'>
                        <div className='absolute top-0 rounded-lg w-full h-[50%] bg-cover' style={{ backgroundImage: 'url("https://res.cloudinary.com/dhq9acwqr/image/upload/v1667098369/Img/yoututosjefse_ge_wxuqkg.jpg")' }}>
                        </div>
                        <section className="flex justify-center container h-auto mx-auto items-center bg-glass2 rounded-none mt-20 py-10">
                            <div >
                                <div className="w-full px-4 flex justify-center items-center">
                                    <img
                                        alt="..."
                                        src="https://res.cloudinary.com/dhq9acwqr/image/upload/v1691598440/nft/Default_megapolis_cyberpunk_lighting_bar_single_girl_with_head_1_19f3a4d7-9993-44f6-b48e-71f67c289e02_1_lvtpfa.jpg"
                                        className="shadow-xl rounded-full  align-center border-none w-52 h-52 overflow-hidden object-cover"
                                    />
                                </div>
                                <div className="flex flex-col w-full mb-6 shadow-xl">
                                    <div className="px-6 flex flex-col justify-center items-center">
                                        <div className="inline-block justify-center items-end">
                                            <div className="px-4">
                                                <div className="flex justify-center py-4 lg:pt-4 pt-8">
                                                    <div className="flex-1 mr-4 p-3 text-center">
                                                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                                                            22
                                                        </span>
                                                        <span className="text-sm text-blueGray-400">Friends</span>
                                                    </div>
                                                    <div className="flex-1 mr-4 p-3 text-center">
                                                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                                                            10
                                                        </span>
                                                        <span className="text-sm text-blueGray-400">Photos</span>
                                                    </div>
                                                    <div className="flex-1 lg:mr-4 p-3 text-center">
                                                        <span className="text-xl font-bold block uppercase tracking-wide text-blueGray-600">
                                                            89
                                                        </span>
                                                        <span className="text-sm text-blueGray-400">Comments</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <button
                                                className="bg-pink-500 active:bg-pink-600 uppercase text-white font-bold hover:shadow-md shadow px-4 py-2 rounded focus:outline-none sm:mr-2 mb-1 ease-linear transition-all duration-150"
                                                type="button"
                                            >
                                                Connect
                                            </button>
                                        </div>
                                        <div className="text-center mt-12">
                                            <h3 className="text-4xl font-semibold leading-normal mb-2 text-blueGray-700">
                                                Jenna Stones
                                            </h3>
                                            <div className="text-sm leading-normal mt-0 mb-2 text-blueGray-400 font-bold uppercase">
                                                <i className="fas fa-map-marker-alt mr-2 text-lg text-blueGray-400" />
                                                Los Angeles, California
                                            </div>
                                            <div className="mb-2 text-blueGray-600 mt-10">
                                                <i className="fas fa-briefcase mr-2 text-lg text-blueGray-400" />
                                                Solution Manager - Creative Tim Officer
                                            </div>
                                            <div className="mb-2 text-blueGray-600">
                                                <i className="fas fa-university mr-2 text-lg text-blueGray-400" />
                                                University of Computer Science
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
                <div className='flex flex-1 flex-col gap-5 rounded-xl overflow-hidden bg-glass px-14 py-10 '>
                    <TodoProvider>
                        <App />
                    </TodoProvider>
                </div>
            </div>
        </>
    )
}

export default Dashboard
import TodoCounter from "../components/TodoCounter";
import TodoSearch from "../components/TodoSearch";
import TodoList from "../components/TodoList";
import TodoItem from "../components/TodoItem";
import CreateTodoButton from "../components/CreateTodoButton";
import EmptyTodo from "../components/EmptyTodo";
import Loading from "../components/Loading";
import { Modal } from "../components/Modal";
import { TodoContext } from '../context/TodoContext'
import { useContext } from "react";
import { OffChecked } from "../icons/TodoItemIcons";
import { v4 } from 'uuid'
import Error from "../components/notif/Error";
import { useForm } from 'react-hook-form'
import { createTodo } from '../Utils/createTodo'
import { getPosts } from "../Utils/getTodos";

function App() {
    const { loading,
        error, setSearchValue, setTodos, amountTodosCompleted, amountTodos, searchetTodos, completedTodo, offCompletedTodo, deleteTodo, searchValue, openModal, setOpenModal } = useContext(TodoContext)
    const { register, handleSubmit } = useForm({
        defaultValues:{
            title: ''
        }
    })
    async function onSubmitForm(data) {
        await createTodo(data)
        await getPosts().then(data => setTodos(data))
        setOpenModal(!openModal)
        
        // getPosts()
    }


    

    
    const classOPtions = {
        Iniciado: 'border-amber-600 bg-inProgresh border-2 rounded-xl',
        Finalizado: 'border-green-600 bg-finished border-2 rounded-xl',
        Hecho: 'border-blue-600 bg-done border-2 rounded-xl',
        PLanificado: 'border-sky-700 bg-planned border-2 rounded-xl'
    }
    //setOpenModal
    // className={'border-amber-600 bg-inProgresh border-2 rounded-xl'}
    // className={'border-green-600 bg-finished border-2 rounded-xl'}
    // className={'border-blue-600 bg-done border-2 rounded-xl'}

    function handleClick() {
        setOpenModal(!openModal)
    }
    function handleClickCancelled(){
        store.addNotification({
            title: 'Dropbox',
            message: 'Files were synced',
            type: 'default',                         // 'default', 'success', 'info', 'warning'
            container: 'bottom-left',                // where to position the notifications
            animationIn: ["animated", "fadeIn"],     // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"],   // animate.css classes that's applied
            dismiss: {
              duration: 3000 
            }
        })
    }
    return (
        <>
            <TodoCounter completed={amountTodosCompleted} total={amountTodos} />
            <TodoSearch searchValue={searchValue} setSearchValue={setSearchValue} />
            <TodoList >
                {loading && <Loading />}
                {error && <Error />}
                {(!loading && searchetTodos.length === 0) && <EmptyTodo />}
                {!loading && searchetTodos.map((todo) => (
                    <TodoItem
                        className={classOPtions[todo.status]}
                        text={todo.title}
                        key={v4()}
                        completed={todo.isCompleted}
                        onComplete={() => completedTodo(todo)}
                        offComplete={() => offCompletedTodo(todo)}
                        onDelete={() => deleteTodo(todo._id)}
                    />
                ))
                }
            </TodoList>
            <CreateTodoButton handleClick={handleClick} />
            {openModal && (
                <Modal>
                    <div

                        id="default-modal"
                        data-modal-show="true"
                        aria-hidden="true"
                        className="overflow-x-hidden flex bg-glass3 overflow-y-auto fixed h-modal md:h-full top-4 left-0 right-0 md:inset-0 z-50 justify-center items-center"
                    >
                        <div className="relative w-full max-w-2xl px-4 h-full md:h-auto">
                            {/* Modal content */}
                            <div className="bg-white shadow-3xl  rounded-lg relative dark:bg-gray-700">
                                {/* Modal header */}
                                <div className="flex items-start justify-between p-5 border-b rounded-t dark:border-gray-600">
                                    <h3 className="text-gray-900 text-xl lg:text-2xl font-semibold dark:text-white">
                                        Regitra tu tarea
                                    </h3>
                                    <button
                                        type="button"
                                        data-modal-toggle="default-modal"
                                        className="bg-red-500 px-4 py-2 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center"
                                        onClick={handleClick}
                                    >
                                        <OffChecked />
                                    </button>
                                </div>
                                {/* Modal body */}
                                <form onSubmit={handleSubmit(onSubmitForm)}>
                                    <div className="p-6 space-y-6">
                                        <div className="mb-6">
                                            <label
                                                htmlFor="large-input"
                                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                            >
                                                Titulo
                                            </label>
                                            <input
                                                {...register('title')}
                                                type="text"
                                                id="large-input"
                                                className="block w-full p-4 text-gray-900 border border-gray-300 rounded-lg bg-gray-50 sm:text-md focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            />
                                        </div>
                                        <div className="mb-6">
                                            <label
                                                htmlFor="countries"
                                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                            >
                                                Selecciona el estado de tu tarea
                                            </label>
                                            <select
                                                {...register('status')}
                                                id="countries"
                                                className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                            >
                                                <option>PLanificado</option>
                                                <option>Iniciado</option>
                                                <option>Hecho</option>
                                                <option>Finalizado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="flex w-full space-x-2 items-center p-6 border-t border-gray-200 rounded-b dark:border-gray-600">
                                        <button
                                            data-modal-toggle="default-modal"
                                            type="submit"
                                            className="text-white flex-1 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                        >
                                            aceptar
                                        </button>
                                        <button
                                            onClick={handleClickCancelled}
                                            data-modal-toggle="default-modal"
                                            type="button"
                                            className="text-gray-500 flex-1 bg-white hover:bg-gray-100 focus:ring-4 focus:ring-gray-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600"
                                        >
                                            cancelar
                                        </button>
                                    </div>
                                </form>
                                {/* Modal footer */}

                            </div>
                        </div>
                    </div>
                </Modal>
            )}
        </>
    )
}

export default App
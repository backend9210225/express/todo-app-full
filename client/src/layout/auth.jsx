/* eslint-disable react/prop-types */

// import { useState } from "react"
import { useForm } from "react-hook-form"


function Auth({ onChildEvent }) {
    // const [emitData, setEmitData] = useState()
    const { register, handleSubmit } = useForm()

    const onSubmit = (data) => { onChildEvent(data) }


    return (
        <>
            <div className="flex justify-center items-center">
                <div className="card glass-bg w-[30%] rounded-2xl mx-auto flex flex-col items-center p-1">
                    <div className="border-2 p-10 border-cyan-500 w-full h-full rounded-2xl mx-auto bg-black">
                        <div className="pb-10 flex justify-center items-center">
                            <svg className="border-2 rounded-full p-10 border-cyan-500  w-[48%] h-[48%] text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24">
                                <path d="M19 5h-1.382l-.171-.342A2.985 2.985 0 0 0 14.764 3H9.236a2.984 2.984 0 0 0-2.683 1.658L6.382 5H5a3 3 0 0 0-3 3v11a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V8a3 3 0 0 0-3-3Zm-3.5 7a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Z" fill="cyan" />
                            </svg>
                        </div>

                        <div className="w-full">
                            <form className="w-full" onSubmit={handleSubmit(onSubmit)}>
                                <div className="mb-6">
                                    <label
                                        htmlFor="email"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        User or Email
                                    </label>
                                    <input
                                        {...register("userName")}
                                        type="email"
                                        id="email"
                                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="john.doe@company.com"
                                        required=""
                                    />
                                </div>
                                <div className="mb-6">
                                    <label
                                        htmlFor="password"
                                        className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                                    >
                                        Password
                                    </label>
                                    <input
                                        {...register("password")}
                                        type="password"
                                        id="password"
                                        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="•••••••••"
                                        required=""
                                    />
                                </div>

                                <div className="flex justify-between items-center mb-6">
                                    <div className="flex">
                                        <div className="flex items-center h-5">
                                            <input
                                                id="remember"
                                                type="checkbox"
                                                defaultValue=""
                                                className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800"
                                                required=""
                                            />
                                        </div>
                                        <label
                                            htmlFor="remember"
                                            className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                                        >
                                            remind me{" "}

                                            .
                                        </label>
                                    </div>

                                    <a href="#" className="text-blue-600 hover:underline dark:text-blue-500">
                                        forgot password
                                    </a>
                                </div>

                                <div className="mb-6">
                                    <button
                                        type="submit"
                                        className="w-full text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                    >
                                        Submit
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Auth

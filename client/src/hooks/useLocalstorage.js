/* eslint-disable no-unused-vars */
import { useState, useEffect } from "react";
import axios from "axios";
import { server } from "../Utils/helper";
// function useLocalstorage(key, initialValue) {
function useLocalstorage(key, initialValue) {
  // * localStorage manipulation

  const [item, setItem] = useState(initialValue);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  useEffect(() => {
    const todos = fetchData();
    setItem(todos);
    // setTimeout(() => {
    //   try {
    //     const localStorageItem = localStorage.getItem(key);
    //     let parsetItem;
    //     if (!localStorageItem) {
    //       localStorage.setItem(key, JSON.stringify(initialValue));
    //       parsetItem = initialValue;
    //     } else {
    //       parsetItem = JSON.parse(localStorageItem);
    //       setItem(parsetItem);
    //     }
    //     setLoading(false);
    //   } catch (error) {
    //     setLoading(false);
    //     setError(true);
    //   }
    // }, 7000);
  }, [item, initialValue]);

  const saveItem = (value) => {
    localStorage.setItem(key, JSON.stringify(value));
    setItem(value);
  };

  return { item, saveItem, loading, error };
}

async function fetchData() {
  try {
    const data = await axios.get(`${server.baseUrl}/todos`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
}

export { useLocalstorage };

import { TodoStatus } from '../todo.entity';
export declare class TodoDto {
    title: string;
    status?: TodoStatus;
}
export declare class UpdateTodoDto {
    title?: string;
    status?: TodoStatus;
}

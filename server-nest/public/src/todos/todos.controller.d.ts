import { Response } from 'express';
import { TodosService } from './todos.service';
import { TodoDto, UpdateTodoDto } from './dto/todo.dto';
export declare class TodosController {
    private todosServices;
    constructor(todosServices: TodosService);
    getAllTodos(res: Response): Promise<Response<any, Record<string, any>>>;
    getAllTodo(id: string): Promise<import("./schemas/todo.schema").Todo>;
    createTodo(newTodo: TodoDto, res: Response): Promise<Response<any, Record<string, any>>>;
    updateTodo(id: string, newData: UpdateTodoDto, res: Response): Promise<Response<any, Record<string, any>>>;
    deleteTodo(id: string, res: Response): Promise<Response<any, Record<string, any>>>;
}

export declare enum TodoStatus {
    PLANNED = "PLanificado",
    FINISHED = "Finalizado",
    Iniciado = "Iniciado",
    DONE = "Hecho"
}
export declare class Todo {
    id: string;
    title: string;
    isCompleted: boolean;
    status: TodoStatus;
}

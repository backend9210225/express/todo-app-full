import { Model } from 'mongoose';
import { TodoStatus } from './todo.entity';
import { UpdateTodoDto } from './dto/todo.dto';
import { Todo } from './schemas/todo.schema';
export declare class TodosService {
    private todoModel;
    constructor(todoModel: Model<Todo>);
    getAllTodos(): Promise<Todo[]>;
    getTodo(id: string): Promise<Todo>;
    createTodo(title: string, status?: TodoStatus): Promise<Todo>;
    updateTodo(id: string, data: UpdateTodoDto): Promise<any>;
    deleteTodo(id: string): Promise<void>;
}

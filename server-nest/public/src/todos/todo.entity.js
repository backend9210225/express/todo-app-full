"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Todo = exports.TodoStatus = void 0;
var TodoStatus;
(function (TodoStatus) {
    TodoStatus["PLANNED"] = "PLanificado";
    TodoStatus["FINISHED"] = "Finalizado";
    TodoStatus["Iniciado"] = "Iniciado";
    TodoStatus["DONE"] = "Hecho";
})(TodoStatus || (exports.TodoStatus = TodoStatus = {}));
class Todo {
}
exports.Todo = Todo;
//# sourceMappingURL=todo.entity.js.map
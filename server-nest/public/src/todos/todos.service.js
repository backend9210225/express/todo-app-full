"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TodosService = void 0;
const mongoose_1 = require("mongoose");
const common_1 = require("@nestjs/common");
const todo_entity_1 = require("./todo.entity");
const mongoose_2 = require("@nestjs/mongoose");
const todo_schema_1 = require("./schemas/todo.schema");
const uuid_1 = require("uuid");
let TodosService = class TodosService {
    constructor(todoModel) {
        this.todoModel = todoModel;
    }
    async getAllTodos() {
        return await this.todoModel.find().exec();
    }
    async getTodo(id) {
        return await this.todoModel.findById(id).exec();
    }
    async createTodo(title, status) {
        const st = status ? status : todo_entity_1.TodoStatus.Iniciado;
        const todo = {
            id: (0, uuid_1.v4)(),
            title,
            isCompleted: false,
            status: st,
        };
        try {
            const createdTodo = new this.todoModel(todo);
            return await createdTodo.save();
        }
        catch (error) {
            return error;
        }
    }
    async updateTodo(id, data) {
        try {
            const updatedTodo = await this.todoModel.findByIdAndUpdate(id, data, {
                new: true,
            });
            return updatedTodo;
        }
        catch (error) {
            return error;
        }
    }
    async deleteTodo(id) {
        const findTodo = await this.getTodo(id);
        await this.todoModel.deleteOne({ id: findTodo.id });
        return;
    }
};
exports.TodosService = TodosService;
exports.TodosService = TodosService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_2.InjectModel)(todo_schema_1.Todo.name)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], TodosService);
//# sourceMappingURL=todos.service.js.map
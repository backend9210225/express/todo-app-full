import { ConfigService } from '@nestjs/config';
export declare class AppController {
    private env;
    constructor(env: ConfigService);
    hello(): string;
}

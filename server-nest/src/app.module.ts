/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TodosModule } from './todos/todos.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { enviroments } from '../config/database.config';
// import { TypeOrmModule } from "@nestjs/typeorm"

@Module({
  imports: [
    TodosModule,
    MongooseModule.forRoot(
      'mongodb+srv://krodriguezcondori34:QJii0zAfKHH5nvEk@todo-test.trrxw7h.mongodb.net/',
    ),
    ConfigModule.forRoot({
      envFilePath: enviroments[process.env.NODE_ENV] || '.env.dev',
      ignoreEnvFile: true,
      isGlobal: true,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

/* eslint-disable prettier/prettier */
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { TodoStatus } from './todo.entity';
import { UpdateTodoDto } from './dto/todo.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Todo } from './schemas/todo.schema';
import { v4 as uidd } from 'uuid';

@Injectable()
export class TodosService {
  constructor(@InjectModel(Todo.name) private todoModel: Model<Todo>) {}
  //   @Get()
  async getAllTodos(): Promise<Todo[]> {
    return await this.todoModel.find().exec();
  }

  //   @Get('id')
  async getTodo(id: string): Promise<Todo> {
    return await this.todoModel.findById(id).exec(); //this.todos.find((item) => item.id === id);
  }

  //   @Post()
  async createTodo(title: string, status?: TodoStatus): Promise<Todo> {
    const st = status ? status : TodoStatus.Iniciado;
    const todo: Todo = {
      id: uidd(),
      title,
      isCompleted: false,
      status: st,
    };
    // this.todos.push(todo);
    try {
      const createdTodo = new this.todoModel(todo);
      return await createdTodo.save();
    } catch (error) {
      return error;
    }
  }

  //   @Patch('id')
  async updateTodo(id: string, data: UpdateTodoDto) {
    //const todo = this.getTodo(id);
    try {
      const updatedTodo = await this.todoModel.findByIdAndUpdate(id, data, {
        new: true,
      });
      return updatedTodo;
    } catch (error) {
      return error      
    }
  }

  //   @Delete('id')
  async deleteTodo(id: string) {
    const findTodo = await this.getTodo(id);
    await this.todoModel.deleteOne({ id: findTodo.id }); //this.todos.filter((todo) => todo.id !== id);
    return;
  }
}

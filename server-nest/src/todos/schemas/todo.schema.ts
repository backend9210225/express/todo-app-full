/* eslint-disable prettier/prettier */
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { TodoStatus } from '../todo.entity';

// import { Todo } from '../todo.entity'
export type todoDocument = HydratedDocument<Todo>;

@Schema()
export class Todo {
  @Prop()
  id: string;
  @Prop()
  title: string;
  @Prop()
  isCompleted: boolean;
  @Prop()
  status: TodoStatus;
}

export const todoSchema = SchemaFactory.createForClass(Todo);

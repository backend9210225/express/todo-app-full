/* eslint-disable prettier/prettier */

import { IsOptional, IsIn } from 'class-validator';

import { TodoStatus } from '../todo.entity';
export class TodoDto {
  title: string;
  @IsOptional()
  @IsIn([TodoStatus.DONE, TodoStatus.FINISHED, TodoStatus.Iniciado, TodoStatus.PLANNED])
  status?: TodoStatus;
}

export class UpdateTodoDto {
  @IsOptional()
  title?: string;
  @IsOptional()
  @IsIn([TodoStatus.DONE, TodoStatus.FINISHED, TodoStatus.Iniciado, TodoStatus.PLANNED])
  status?: TodoStatus;
}

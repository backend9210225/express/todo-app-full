/* eslint-disable prettier/prettier */
export enum TodoStatus {
  PLANNED = 'PLanificado',
  FINISHED = 'Finalizado',
  Iniciado = 'Iniciado',
  DONE = 'Hecho',
}

export class Todo {
  id: string;
  title: string;
  isCompleted: boolean;
  status: TodoStatus;
}

/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get,
  Patch,
  Post,
  Param,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { TodosService } from './todos.service';
import { TodoDto, UpdateTodoDto } from './dto/todo.dto';

// import { ObjectId } from 'mongoose';
@Controller('todos')
export class TodosController {
  constructor(private todosServices: TodosService) {}
  @Get()
  async getAllTodos(@Res() res: Response) {
    const todos = await this.todosServices.getAllTodos();
    try {
      if (!todos)
        return res
          .status(400)
          .json({ message: 'todos not found, create a first todo' });
      return res.status(200).json(todos);
    } catch (error) {
      return res.status(401).json({ error: error });
    }
  }
  @Get(':id')
  getAllTodo(@Param('id') id: string) {
    return this.todosServices.getTodo(id);
  }

  @Post()
  async createTodo(@Body() newTodo: TodoDto, @Res() res: Response) {
    const todo = await this.todosServices.createTodo(
      newTodo.title,
      newTodo.status,
    );
    return res.status(200).json(todo);
  }

  @Patch(':id')
  async updateTodo(
    @Param('id') id: string,
    @Body() newData: UpdateTodoDto,
    @Res() res: Response,
  ) {
    try {
      console.log(newData);
      const todo = await this.todosServices.updateTodo(id, newData);
      return res.status(200).json(todo);
    } catch (error) {
      return res.status(401).json({ "error": error });
    }
  }

  @Delete(':id')
  async deleteTodo(@Param('id') id: string, @Res() res: Response) {
    await this.todosServices.deleteTodo(id);
    return res.status(200).json({ message: `todo con ${id} eliminado` });
  }
}

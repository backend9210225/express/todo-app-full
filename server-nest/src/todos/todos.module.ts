/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { TodosController } from './todos.controller';
import { TodosService } from './todos.service';
import { MongooseModule } from '@nestjs/mongoose'
import { Todo,todoSchema } from './schemas/todo.schema'

@Module({
  imports:[ MongooseModule.forFeature([{name: Todo.name, schema: todoSchema}])],
  controllers: [TodosController],
  providers: [TodosService],
})
export class TodosModule {}

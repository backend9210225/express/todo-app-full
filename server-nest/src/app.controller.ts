/* eslint-disable prettier/prettier */
import { Controller, Get } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
// import { enviroments } from '../config/database.config';
@Controller('/')
export class AppController {
  constructor(private env: ConfigService) {}
  @Get()
  hello(): string {
    const db = this.env.get('DATABASE_URI');
    const envN = this.env.get('NODE_ENV');
    return `Hello world from home ${db} ${envN}`;
  }
}

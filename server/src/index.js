import app from "./app.js";
import { config } from "./configurations/config.js";
import { connect } from './db/connectorDB.js'
const port = config.port;

app.listen(port);

console.log(`app listen on port ${port}`);

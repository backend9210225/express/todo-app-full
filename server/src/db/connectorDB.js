import { MongoClient } from "mongodb";

export async function connect(uri) {
  try {
    const mongoclient = new MongoClient(uri);
    console.log("");
    await mongoclient.connect();
    console.log("Successfully connected to MongoDB Atlas!");
    return mongoclient;
  } catch (error) {
    console.error("Connection to mongoDB atlas failed", error);
    process.exit();
  }
}

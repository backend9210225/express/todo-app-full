import express from "express";
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
const app = express()


app.use(morgan('dev'))
app.use(express.json())
app.use(cookieParser())
app.use(bodyParser.json())

export default app